<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
        	[
                'name'=> 'Admin',
            	'email'=> 'admin@gmail.com',
            	'password'=> bcrypt('admin'),
            	'is_admin'=> 1
            ],

            [
                'name'=> 'Petras',
                'email'=> 'petras@gmail.com',
                'password'=> bcrypt(123456),
                'is_admin'=> 0
            ],

            [
                'name'=> 'Antanas',
                'email'=> 'antanas@gmail.com',
                'password'=> bcrypt(123456),
                'is_admin'=> 0
            ],

            [
                'name'=> 'Jonas',
                'email'=> 'jonas@gmail.com',
                'password'=> bcrypt(123456),
                'is_admin'=> 0
            ],
       	]);
    }
}
