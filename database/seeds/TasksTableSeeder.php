<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
        	[
        		'name' => 'Wash dishes'
            ],

            [
        		'name' => 'Take dog out'
            ],

            [
        		'name' => 'Clean windows'
            ],

            [
        		'name' => 'Wash the floor'
            ],

            [
        		'name' => 'Make dinner'
            ],

            [
        		'name' => 'Hang a picture'
            ],

            [
                'name' => 'Wash car'
            ],

            [
                'name' => 'Fix tv'
            ],

            [
                'name' => 'Read a book'
            ],

            [
                'name' => 'Take out garbage'
            ],

            [
                'name' => 'Go shopping'
            ],

            [
                'name' => 'Do homework'
            ],
       	]);
    }	
}
