-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2017 m. Kov 24 d. 11:46
-- Server version: 5.7.14
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `work_list`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(9, '2014_10_12_000000_create_users_table', 1),
(10, '2014_10_12_100000_create_password_resets_table', 1),
(11, '2017_03_21_151601_create_task_table', 1),
(12, '2017_03_21_161124_add_admin_to_users', 1);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Wash dishes', 2, NULL, '2017-03-24 06:49:14'),
(2, 'Take dog out', 2, NULL, '2017-03-24 06:49:18'),
(3, 'Clean windows', 2, NULL, '2017-03-24 06:49:23'),
(4, 'Wash the floor', 2, NULL, '2017-03-24 06:49:27'),
(5, 'Make dinner', 3, NULL, '2017-03-24 06:49:33'),
(6, 'Hang a picture', 3, NULL, '2017-03-24 06:49:37'),
(7, 'Wash car', 3, NULL, '2017-03-24 06:49:41'),
(8, 'Fix tv', 3, NULL, '2017-03-24 06:49:45'),
(9, 'Read a book', 4, NULL, '2017-03-24 06:49:49'),
(10, 'Take out garbage', 4, NULL, '2017-03-24 06:49:53'),
(11, 'Go shopping', 4, NULL, '2017-03-24 06:49:58'),
(12, 'Do homework', 4, NULL, '2017-03-24 06:50:02');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `task_id` int(10) UNSIGNED DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `task_id`, `is_admin`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$aYvnTExBxn3fEJYhxm/b6.52ZQwwdCMq3kYt/UcjMDVfMOnrDjcPu', 'WnlzMWAY5U92SDE98KBFMy3CC79fPOseZ26SzNfiFzeACptB9u245YVomgHf', NULL, NULL, NULL, 1),
(2, 'Petras', 'petras@gmail.com', '$2y$10$JtRxKRbVOq//NK27efGrDexhExbJuwvo4qQ7eCylRk3hOaZwE3YLu', 'S7EOS8DmmuyRX9gB8jzgyTGDllKyLCaJPdbHWNiUQQDOhj9MRyARTZBPqxSO', NULL, NULL, NULL, 0),
(3, 'Antanas', 'antanas@gmail.com', '$2y$10$v7Lc5.zgoyq/Oy2GJuX9BuKyOy8zyvW9i90qCux1TvD8PZPNaCI9O', 'hRFwmtZW2TWL9gBYGw13yjyEBFFO6GIFQuGvun22o9Yqyp1PiuQRyq8lWUa5', NULL, NULL, NULL, 0),
(4, 'Jonas', 'jonas@gmail.com', '$2y$10$L4Uli2nUXJPfWsOUUxBuwOxG2/WKKw0ug8ZYfws7drSm4jnyrc7ne', '4HTfoAMfpRZMe79j4iaaEeYDNuALiyj59MeLg3XV7LnTUWzhApmzkShanfB6', NULL, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_task_id_foreign` (`task_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
