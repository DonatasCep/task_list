# Tasks List site #

This site was created using:

* Laravel framework; 
* HTML;
* CSS;
* PHP; 
* MySql database;

### Site has this elements ###

* Connection with MySql Database
* Tasks List
* Admin and users

### How set up it? ###

* Inside Mysql create database work_list also db is main folder
* run command in yuor command prompt "git clone https://DonatasCep@bitbucket.org/DonatasCep/task_list.git"
* run command "composer install"
* rename ".env.example" file in main directory to ".env"
* run "php artisan key:generate"
* open ".env" file, change DB_DATABASE=homestead DB_USERNAME=homestead
	DB_PASSWORD=secret to DB_DATABASE=work_list DB_USERNAME=root DB_PASSWORD=
* run command "php artisan migrate"
* run command "php artisan db:seed --class=UserTableSeeder"
* run command "php artisan db:seed --class=TasksTableSeeder"

### Admin and users login ###

* Admin login: admin@gmail.com, pw: admin
* Users login:
 petras@gmail.com, pw: 123456
 antanas@gmail.com, pw: 123456
 jonas@gmail.com, pw: 123456