<?php

namespace App\Http\Controllers;

use Auth;
use App\Task;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class TaskController extends Controller
{   
    public function __construct ()
    {

        $this->middleware('auth', ['except' => ['index']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {   
        if (Auth::check() && !Auth::user()->isAdmin()) {
            $task = Auth::user()->getId();
            $tasks = Task::where('user_id', $task)->get();
        } else {
            $tasks = Task::all();
        }
        return view('task.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $users = User::all();
        $selectUsers = array();

        foreach ($users as $user) {
            $selectUsers[$user->id] = $user->name;
        }

        return view('task.create', compact('selectUsers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = Task::create($request->all());
        return redirect()->route('task.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);

        $users = User::all();
        $selectUsers = array();

        foreach ($users as $user) {
            $selectUsers[$user->id] = $user->name;
        }

        return view('task.create', compact('task', 'selectUsers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Task::find($id)->update($request->all());

        return redirect()->route('task.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Task::find($id)->delete();

        return redirect()->route('task.index');
    }
}
