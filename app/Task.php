<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = [
    	'name', 'user_id',
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
