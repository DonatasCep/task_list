@extends('main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                   @if(Auth::guest())
                        You are quest, please login
                    @else
                        You don't have permission
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection