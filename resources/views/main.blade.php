@include("layout.header")

<main>
	<div class="container">
	
		@yield("content") 
	</div>
</main>

@include("layout.footer")