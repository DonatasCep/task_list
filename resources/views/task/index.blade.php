@extends('main')

@section('content')

	<h1>Task list</h1>
	
	@if(!Auth::guest() && Auth::user()->is_admin) 
    <a href="{{ route('task.create') }}" class="btn btn-info">Add Task</a>
  @else
    
  @endif

  <div class="row">
    <div class="card"> 
      <ul>
        @foreach($tasks as $task)
          <li class="col-md-3 col-sm-6 col-xs-12">
            <div class="card-block">
							<h4>{{ $task->name }}</h4>
              @if(!Auth::guest() && Auth::user()->is_admin)
								<a href="{{ route('task.edit', $task->id) }}" class="btn btn-success btn-xs">Edit</a><br>
              @else

              @endif

              @if(Auth::user() && !Auth::user()->is_admin )
                <form>
                  <label for="option1">Task completed </label>
                  <input type="checkbox" id="option1">
                </form>
              @else

              @endif
            </div>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
@endsection