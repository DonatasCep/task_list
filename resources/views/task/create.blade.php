@extends('main')

@section('content')

		@if(isset($task))
		{{ Form::open(['route' => ['task.update', $task->id], "method" => "POST"]) }}
		<input type="hidden" name="_method" value="PUT">
		@else

		{!! Form::open(['url' => '/task', 'method' => "POST"]) !!}
		@endif
		<h3>Create task</h3>

		{{ Form::label('Name') }}<br>
		@if(isset($task))
			{{ Form::text('name', $task->name, ['class' => 'form-control']) }}
		@else
			{{ Form::text('name','',['class' => 'form-control']) }}<br>
		@endif

		{{ Form::label('Users') }}<br>
		@if(isset($task))
			{{ Form::select('user_id', $selectUsers, $task->user_id, ['class' => 'btn btn-default dropdown-toggle']) }}<br>
		@else
			{{ Form::select('user_id', $selectUsers, ['class' => 'btn btn-default dropdown-toggle']) }}
		@endif
	{!! Form::submit('Save', ['class' => 'btn btn-info btn-sm btnSave']); !!}
	{!! Form::close() !!}

	@if(isset($task))

	{{ Form::open(['route' => ['task.destroy', $task->id], "method" => "POST"]) }}
		<input type="hidden" name="_method" value="DELETE">

		{{ csrf_field() }}

	
		{!! Form::submit('Detele', ['class' => 'btn btn-danger btn-sm btnDelete']); !!}
	{!! Form::close() !!}

	@endif

@endsection